/*!

=========================================================
* Vue Argon Design System - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import './registerServiceWorker'
import { firestorePlugin } from 'vuefire';
import firebase from 'firebase/app';
import 'firebase/firestore'

// import { db } from './db'

export default {
  data() {
    return {
      documents: [],
    }
  },

  firestore: {
    documents: db.collection('documents'),
  },
}

export const db = firebase
  .initializeApp({ projectId: 'Jokosun' })
  .firestore()

const { TimeStamp, GeoPoint } = firebase.firestore
export { TimeStamp, GeoPoint }

Vue.config.productionTip = false;
Vue.use(Argon);
Vue.use(firestorePlugin);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");

// // RecentDocuments.vue
// import { db } from './db'

// export default {
//   data() {
//     return {
//       documents: [],
//     }
//   },

//   firestore: {
//     documents: db.collection('documents'),
//   },
// }


// // UserProfile.vue
// const users = db.collection('users')

// export default {
//   props: ['id'],
//   data() {
//     return {
//       user: null,
//     }
//   },

//   watch: {
//     id: {
//       // call it upon creation too
//       immediate: true,
//       handler(id) {
//         this.$bind('user', users.doc(id))
//       },
//     },
//   },
// }


this.$bind('user', users.doc(this.id)).then(user => {
  // user will point to the same property declared in data:
  // this.user === user
})

this.$bind('documents', documents.where('creator', '==', this.id)).then(documents => {
  // documents will point to the same property declared in data:
  // this.documents === documents
})





.controller('ShoppingCtrl', function($scope, $http) {
  var url = 'Jokosun/items.json';

  $scope.items = getItems();

  $scope.addItem = function() {
    var name = prompt("Que devez-vous acheter?");
    if (name) {
      var postData = {
        "name": name
      };
      $http.post(url, postData).success(function(data) {
        $scope.items = getItems();
      });
    }
  };

  function getItems() {
    var items = [];
    $http.get(url).success(function(data) {
      angular.forEach(data, function(value, key) {
        var name = {name: value.name};
          items.push(name);
      });
    });

    return items;
  }

});